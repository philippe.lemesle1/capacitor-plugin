package care.hoppen.plugins;

import android.app.Activity;
import android.view.View;

import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.CapacitorPlugin;

@CapacitorPlugin(name = "FullscreenManagement")
public class FullscreenManagementPlugin extends Plugin {
    @PluginMethod()
    public void setFullscreen(PluginCall call) {
        final Activity activity = this.getActivity();
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
            }
        });
    }

    @PluginMethod()
    public void unsetFullscreen(PluginCall call) {
        final Activity activity = this.getActivity();
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
            }
        });
    }
}
