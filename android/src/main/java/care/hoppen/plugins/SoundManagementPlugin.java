

////////////////////////////////

package care.hoppen.plugins;

import android.content.Context;
import android.media.AudioManager;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.CapacitorPlugin;


@CapacitorPlugin(name = "SoundManagement")
public class SoundManagementPlugin extends Plugin {
    @PluginMethod()
    public void increaseVolume(PluginCall call) {
        AudioManager audioManager = (AudioManager) this.getContext().getSystemService(Context.AUDIO_SERVICE);
        audioManager.adjustVolume(AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
    }

    @PluginMethod()
    public void decreaseVolume(PluginCall call) {
        AudioManager audioManager = (AudioManager) this.getContext().getSystemService(Context.AUDIO_SERVICE);
        audioManager.adjustVolume(AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
    }

    // @PluginMethod()
    // public void toggleMute(PluginCall call) {
    //     AudioManager audioManager = (AudioManager) this.getContext().getSystemService(Context.AUDIO_SERVICE);
    //     audioManager.adjustVolume(AudioManager.ADJUST_TOGGLE_MUTE, AudioManager.FLAG_SHOW_UI);
    // }

    // @RequiresApi(api = Build.VERSION_CODES.M)
    // @PluginMethod()
    // public void isMuted(PluginCall call) {
    //     AudioManager audioManager = (AudioManager) this.getContext().getSystemService(Context.AUDIO_SERVICE);
    //     Boolean isMuted = audioManager.isStreamMute(AudioManager.STREAM_MUSIC);
    //     JSObject res = new JSObject();
    //     res.put("isMuted", isMuted);
    //     call.success(res);
    // }
}
