export interface ExamplePlugin {
  echo(options: { value: string }): Promise<{ value: string }>;
}

export interface DeviceManagementPluginInterface {
  getDeviceMacAddress(): Promise<{ macAddress: string }>;
  // getDeviceSerial(): Promise<{ serial: string }>;
  // getApps(): Promise<{ appList: string }>;
  // launchApp(packageName: { packageName: string }): Promise<void>;
}

export interface SoundManagementPluginInterface {
  increaseVolume(): Promise<void>;
  decreaseVolume(): Promise<void>;
  toggleMute(): Promise<void>;
  isMuted(): Promise<boolean>;
}

export interface FullscreenManagementPluginInterface {
  setFullscreen(): Promise<void>;
  unsetFullscreen(): Promise<void>;
}
