import { registerPlugin } from '@capacitor/core';

import { FullscreenManagementPluginInterface } from '../definitions';

const FullscreenManagementPlugin =
  registerPlugin<FullscreenManagementPluginInterface>(
    'FullscreenManagementPlugin',
    {
      web: () =>
        import('./fullscreen-management-plugin-web').then(
          m => new m.FullscreenManagementPluginWeb(),
        ),
    },
  );

export { FullscreenManagementPlugin };
