import { WebPlugin } from '@capacitor/core';
import { FullscreenManagementPluginInterface } from '../definitions';

export class FullscreenManagementPluginWeb
  extends WebPlugin
  implements FullscreenManagementPluginInterface
{
  constructor() {
    super({
      name: 'FullscreenManagementPlugin',
      platforms: ['web'],
    });
  }

  async setFullscreen(): Promise<void> {
    console.debug('setFullscreen');
  }

  async unsetFullscreen(): Promise<void> {
    console.debug('unsetFullscreen');
  }
}
