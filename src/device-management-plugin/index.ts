import { registerPlugin } from '@capacitor/core';

import { DeviceManagementPluginInterface } from '../definitions';

const DeviceManagementPlugin = registerPlugin<DeviceManagementPluginInterface>(
  'DeviceManagementPlugin',
  {
    web: () =>
      import('./device-management-plugin-web').then(
        m => new m.DeviceManagementPluginWeb(),
      ),
  },
);

export { DeviceManagementPlugin };
