import { WebPlugin } from '@capacitor/core';
import { SoundManagementPluginInterface } from '../definitions';

export class SoundManagementPluginWeb
  extends WebPlugin
  implements SoundManagementPluginInterface
{
  constructor() {
    super({
      name: 'SoundManagementPlugin',
      platforms: ['web'],
    });
  }

  async increaseVolume(): Promise<void> {
    console.debug('increaseVolume');
  }

  async decreaseVolume(): Promise<void> {
    console.debug('decreaseVolume');
  }

  async toggleMute(): Promise<void> {
    console.debug('toggleMute');
  }

  async isMuted(): Promise<boolean> {
    console.debug('isMuted');
    return false;
  }
}
