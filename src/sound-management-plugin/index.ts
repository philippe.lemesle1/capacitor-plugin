import { registerPlugin } from '@capacitor/core';

import { SoundManagementPluginInterface } from '../definitions';

const SoundManagementPlugin = registerPlugin<SoundManagementPluginInterface>(
  'SoundManagementPlugin',
  {
    web: () =>
      import('./sound-management-plugin-web').then(
        m => new m.SoundManagementPluginWeb(),
      ),
  },
);

export { SoundManagementPlugin };
